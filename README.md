# jQueryWrapper

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

## Inspired by
https://vuejsdevelopers.com/2017/05/20/vue-js-safely-jquery-plugin/
